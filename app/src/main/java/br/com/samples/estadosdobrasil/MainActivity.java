package br.com.samples.estadosdobrasil;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import br.com.samples.estadosdobrasil.util.NetworkUtil;

public class MainActivity extends AppCompatActivity {

    final String TAG = "MainActivity";

    ProgressBar progressBarLoading;
    //  TextView tvTextoExibido;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView) findViewById(R.id.clinicaslista);
        // tvTextoExibido = findViewById(R.id.tv_texto_exibido);
        progressBarLoading = findViewById(R.id.pb_loading);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu m) {
        getMenuInflater().inflate(R.menu.main, m);
        return super.onCreateOptionsMenu(m);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_web_service:
                callWebService();
                break;
            case R.id.menu_clear:
                clearText();
                break;
            case R.id.menu_evo:
                callWebServiceEvo();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void callWebServiceEvo() {
        URL url = NetworkUtil.buildUrlCredenciadasEvo();
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void callWebService() {
        Log.d(TAG, "method callWebService");
        URL url = NetworkUtil.buildUrl("stf");
        MinhaAsyncTask task = new MinhaAsyncTask();
        task.execute(url);
    }

    public void mostrarLoading() {
        listView.setVisibility(View.GONE);
        //tvTextoExibido.setVisibility(View.GONE);
        progressBarLoading.setVisibility(View.VISIBLE);
    }

    public void esconderLoading() {
        listView.setVisibility(View.VISIBLE);
        // tvTextoExibido.setVisibility(View.VISIBLE);
        progressBarLoading.setVisibility(View.GONE);
    }

    public void clearText() {
        Log.d(TAG, "method clearText");
        // tvTextoExibido.setText("");
    }


    class MinhaAsyncTask extends AsyncTask<URL, Void, ArrayList<String>> {

        @Override
        protected ArrayList<String> doInBackground(URL... urls) {
            URL url = urls[0];
            Log.d(TAG, "url utilizada: " + url.toString());
            String json = null;
            ArrayList<String> clinicas = new ArrayList<String>();

            JSONObject person = null;
            try {
                json = NetworkUtil.getResponseFromHttpUrl(url);
                Log.d(TAG, "async task retorno: " + json);

                JSONArray jsonArray = new JSONArray(json);
                for (int i = 0; i < jsonArray.length(); i++) {
                    person = (new JSONObject(jsonArray.get(i).toString()));
                    clinicas.add(person.getString("nome_fantasia"));
                }


            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return clinicas;

        }

        @Override
        protected void onPreExecute() {
            mostrarLoading();
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<String> strings) {
            ArrayAdapter<String> adapterClinicas;
            esconderLoading();
            adapterClinicas = new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_list_item_1, strings);
            listView.setAdapter(adapterClinicas);
            Log.d(TAG, "async task retorno: " + strings);
         //   listView.setAdapter((ListAdapter) strings);
            super.onPostExecute(strings);

        }

      /*  @Override
        protected void onPostExecute(String s) {
            esconderLoading();
            if (s == null) {
                tvTextoExibido.setText("Ocorreu um erro");
            } else {
                tvTextoExibido.setText(s);
            }
        }*/
    }


}
